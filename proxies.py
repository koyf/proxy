import select
import socket
import socketserver
import http.server
import logging
from time import sleep
from threading import Thread


logger = logging.getLogger('proxy.proxies')


def proxy_factory():
    class Proxy(http.server.SimpleHTTPRequestHandler):
        def get_client_conn(self):
            client_conn = socket.socket()
            host_port_split = self.path.split(':')
            host_port = host_port_split[0], int(host_port_split[-1])
            try:
                logger.debug('Connection {}: Started'.format(self.client_address))
                client_conn.connect(host_port)
            except socket.error as arg:
                logger.debug('Connection {}: Connection failed'.format(self.client_address))
                self.send_error(404, str(arg))
            return client_conn
  
        def send_data_to_hosts(self, client_conn):
            wait_items = [self.connection, client_conn]
            socket_idle = 0
            while True:
                input_ready, output_ready, exception_ready = select.select(wait_items, [], wait_items, .1)
                if exception_ready:
                    logger.debug('Connection {}: Error'.format(self.client_address))
                    return
                if input_ready:
                    for item in input_ready:
                        data = item.recv(8192)
                        if data:
                            if item is client_conn:
                                local_conn = self.connection
                            else:
                                local_conn = client_conn
                            local_conn.send(data)
                        else:
                            return
                else:
                    if socket_idle < 30:
                        sleep(0.1)
                        socket_idle += 1
                        #print('Connection {}: Waiting for connection'.format(client_conn.fileno()))
                    else:
                        return
  
        def do_HEAD(self):
            super().do_HEAD()
  
        def do_GET(self):
            super().do_GET()
  
        def do_POST(self):
            self.send_response(501)
            self.end_headers()
  
        def do_PUT(self):
            self.send_response(501)
            self.end_headers()
  
        def do_DELETE(self):
            self.send_response(501)
            self.end_headers()
  
        def do_CONNECT(self):
            client_conn = self.get_client_conn()
            try:
                if client_conn:
                    self.log_request(200)
                    self.wfile.write('{} 200 Connection established\nProxy-agent: {}\n\n'
                                     .format(self.protocol_version, self.version_string()).encode())
                    self.send_data_to_hosts(client_conn)
            finally:
                logger.debug('Connection {}: Closing'.format(self.client_address))
                client_conn.close()
                self.connection.close()

        def log_request(self, code='-', size='-'):
            logger.debug('{} "{}" {} {}'.format(self.client_address, self.requestline, code, size))

        def log_error(self, format, *args):
            logger.debug("%s\n" %format%args)

    return Proxy


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


class ProxyBox:
    def __init__(self, port: int, ip: str):
        self.port = port
        self.base_url = ip
        while True:
            try:
                self.server = ThreadedTCPServer((self.base_url, self.port), proxy_factory())
            except OSError:
                logger.debug('Port {} already user. Change to new'.format(self.port))
                self.port += 1
            else:
                break
        self.server_thread = Thread(target=self.server.serve_forever, daemon=True)
  
    def start(self):
        self.server_thread.start()
        logger.debug('Server started')
  
    def shutdown(self):
        self.server.shutdown()
        self.server.server_close()

        logger.debug('Server shutdown')

