import logging
import socket
import datetime
import time
from requests import get

#import pymongo

from proxies import ProxyBox


IFCONFIG_URL = 'https://ifconfig.me'

PORT = 8686


def main():
    date = datetime.datetime.today()

    date_str = str(date).replace(' ', '_')
    date_str = date_str.replace(':', '-')

    logger = logging.getLogger('proxy')
    logger.setLevel(logging.DEBUG)

    formmatter = logging.Formatter('%(asctime)s-%(levelname)s-%(name)s: %(message)s')

    file_handler = logging.FileHandler('logs/' + date_str + '.log')
    file_handler.setFormatter(formmatter)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(formmatter)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    local_ip = socket.gethostbyname(socket.gethostname())

    response = get(IFCONFIG_URL)

    external_ip = response.text

    logger.debug('Response from ifconfig.me')
    logger.debug('status_code: {}'.format(response.status_code))
    logger.debug('text: {}'.format(response.text))

    logger.debug('Running')

    box = ProxyBox(PORT, local_ip)
    box.start()

    logger.debug('Runned on local_ip {}, external_ip {}, PORT {}'.format(local_ip, external_ip, box.port))

    logger.info('Proxy server runned on {}:{}'.format(external_ip, box.port))

    logger.info('Press Ctrl + C to shutdown server')

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            box.shutdown()
            break

if __name__ == "__main__":
    main()